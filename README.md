---
Contents :
---

[[_TOC_]]


# :hourglass_flowing_sand: Time-series-forecasting


### Data set used :
[Pharma Sales Time Series data](https://gitlab.com/GantaChaitra/sales_analysis/-/tree/main/data_set)

* ### Data Set Description:
The dataset is built from the initial dataset consisted of 600000 transactional data collected in 6 years (period 2014-2019), indicating date and time of sale, pharmaceutical drug brand name and sold quantity, exported from Point-of-Sale system in the individual pharmacy. Selected group of drugs from the dataset (57drugs) is classified to the following Anatomical Therapeutic Chemical (ATC) Classification System categories:

M01AB - Anti-inflammatory and antirheumatic products, non-steroids, Acetic acid
derivatives and related substances

M01AE - Anti-inflammatory and antirheumatic products, non-steroids, Propionic acid
derivatives

N02BA - Other analgesics and antipyretics, Salicylic acid and derivatives

N02BE/B - Other analgesics and antipyretics, Pyrazolones and Anilides

N05B - Psycholeptics drugs, Anxiolytic drugs

N05C - Psycholeptics drugs, Hypnotics and sedatives drugs

R03 - Drugs for obstructive airway diseases

R06 - Antihistamines for systemic use

Sales data are resampled to the hourly, daily, weekly and monthly periods.

## Time series data 
It is basically any data that has a consistent distance between x- values. In other words, time series data is sequentially ordered sequence of observations which is quantitatively measured over equally spaced time interval. It contains continuous valuues with a consistent frequency or equal intervals between them. The sequnece of observations recorded st regular intervals could be hourly, daily, weekly, monthy, quaterly, yearly. 

* Properties of Time series data
    * Trend : Long term increasing or decreasing values in data series
    * Seasonality : The repeating short term cycle in a series/periodic functions
    * Stationarity : It is the key characteristic of Time series. A time series is stationary if its statistical properties do not change overtime i.e It has constant mean, variance and covariance. Covariance is independent of time.
    * Level : mean of Time series data
    * Noise/Residuals : Random variation in the data series.
 
